# Rsbar - Really Simple status BAR

This program updates the status bar asynchronously on dwm or any window manager which uses xsetroot as a status bar (`TODO: you'll be able to provide your own way to update`).
It runs in the background, similar to a daemon (although this background process is meant to be attached to a user session), and you can interact with it while it is running.
   
## Usage
   
Start `rsbar`:
```
./rsbar [-r|--refresh] <refresh_ms (0 by default)> [-c|--command] <command_args> --end_command... &
```
   
The `refresh_ms` arguments determines how often the output status bar is updated, in milliseconds.
It will create a string from all the cached outputs of each command and update the status bar with `xsetroot -name <output>`.
   
Each `command` takes arguments which specify what each "box" inside of the status bar should display and requires a `--end_command` to finish parsing its arguments:
```
--command \
   [-n|--name] 'name' # required \
   [-c|--cmd] 'binary from path' #required \
   [-a|--args] 'list' 'of' 'arguments' --end_args #optional, default: none \
   [-t|--timeouts_ms] 500 #optional, default: 0 (never timeout) \
   [-r|--ms_between_refresh] 333 #optional, default: 0 (refresh as fast as possible) \
   [-w|--max_box_width] 40 #optional, default: 0 (no width limit) \
--end_command
```

#### Refresh A Specific Box's Output On Demand

Use this command to update a specific box immediately while the updater is running:
```
./rsbar [-u|--quick_update] <command name(s)>
```
The specific box will be immediately updated.
This is specifically useful when your status bar shows state information like volume, which you know should be immediately updated after you made a keypress, but you don't want to run other slow commands (for example, a weather box which shows the output of a curl request).

   
#### Stopping the background process
   
Use this command to gracefully terminate the process:
```
./rsbar [-k|--kill]
```
You can alternatively send a SIGTERM or a SIGINT to the process, which also should gracefully exit the program.
   
Correct exiting behaviour is important, since I use a POSIX message queue as the IPC mechanism for updating a specific box on demand, and it needs to be deleted.
If you ever get message queue errors from this program after incorrect shutdown, ensure that `/dev/mqueue/rsbar_updater_msq` is deleted, and that your user has the correct permissions (rebooting also fixes it).

## Installation

```
cargo build --release
```
   
System-wide installation:
```
cargo install --path . --root /usr
```
   
User installation:
```
cargo install --path .
```
   
## Examples
   
- A command which shows the time in the sidebar, which is update:
```
rsbar -r 100 --command \
   --cmd 'date' \
   --args '+%0H:%0M:%0S | %02d/%02m/%02y' --end_args \
--end_command
```

- Print three random numbers, which all update at a different time:
```
rsbar --command \
   --name 1 \
   --cmd 'bash' \
   --args '-c' 'echo ${RANDOM}' --end_args \
   --ms_between_refresh 1000 \
--end_command --command \
   --name 2 \
   --cmd 'bash' \
   --args '-c' 'echo ${RANDOM}' --end_args \
   --ms_between_refresh 2000 \
--end_command --command \
   --name 3 \
   --cmd 'bash' \
   --args '-c' 'echo ${RANDOM}' --end_args \
   --ms_between_refresh 3000 \
--end_command
```
