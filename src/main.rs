#[macro_use]
extern crate lazy_static;

use std::process::Command;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use std::thread;
use clap::Arg;
// use std::str::FromStr;
use core::ffi::{c_char,c_void};
use core::mem::MaybeUninit;
//TODO: remove errno, can just get it from core libs
use errno::errno;
use std::collections::{VecDeque,HashMap};
use std::process;
use std::ffi::CString;

//This program:
// - Reads from a config/arguments what to run
// - Runs different
// - Will continuously run and update the status bar asynchronously

lazy_static! {
    static ref MSG_QUEUE_NAME: CString = {
        let user = if let Ok(s) = std::env::var("USER") { s }
        else { "Unknown".to_string() };

        let ret = CString::new(format!("/rsbar_updater_msq_{}", user)).unwrap();
        ret
    };
}

enum IntOrVecInt {
    Int(usize),
    Vec(Vec<usize>)
}

struct State {
    overall_refresh_ms: u64,
    cmds: Vec<Arc<Cmd>>,
    name_to_index: HashMap<String, IntOrVecInt>
}

#[derive(Debug, Clone)]
struct Cmd {
    param: CmdInfo,
    cached_output: Arc<Mutex<Option<String>>>,
    cur_start_index: Arc<Mutex<usize>>,
    remaining_ticks_until_scroll: Arc<Mutex<u64>>,
    remaining_ticks_at_begin: Arc<Mutex<u64>>,
}

#[derive(Debug, Clone)]
struct CmdInfo {
    name: String,
    cmd: String,
    args: Vec<String>,
    timeout_ms: u64,
    //TODO: implement message if timeout or error
    ms_between_refresh: u64,
    max_box_width: u64,
    is_scrolling: bool,
    ticks_until_scroll: u64,
    ticks_at_begin: u64,
    //TODO: addadd  scrolling bool, scrolling rate, etc...
}


#[derive(Debug)]
struct ParseCmdError { }

impl Cmd {
    fn new(matches: clap::ArgMatches) -> Cmd {
        let name = matches.get_one::<String>("name").unwrap().to_owned(); //"name" is required
        let cmd = matches.get_one::<String>("cmd").unwrap().to_owned(); //"cmd" is required
        let args = if let Some(args_matches) = matches.get_many("args") {
            args_matches.cloned().collect()
        } else {
            vec![]
        };
        //"timeout_ms" is defaulted
        let timeout_ms = *matches.get_one::<u64>("timeout_ms").unwrap();
        let ms_between_refresh = *matches.get_one::<u64>("ms_between_refresh").unwrap();
        let max_box_width = *matches.get_one::<u64>("max_box_width").unwrap();
        let is_scrolling = *matches.get_one::<bool>("is_scrolling").unwrap();
        let ticks_until_scroll = *matches.get_one::<u64>("ticks_until_scroll").unwrap();
        let ticks_at_begin = *matches.get_one::<u64>("ticks_at_begin").unwrap();
        let info = CmdInfo {
            name,
            cmd,
            args,
            timeout_ms,
            ms_between_refresh,
            max_box_width,
            is_scrolling,
            ticks_until_scroll,
            ticks_at_begin,
        };
        Cmd {
            param: info,
            cached_output: Arc::new(Mutex::new(None)),
            cur_start_index: Arc::new(Mutex::new(0)),
            remaining_ticks_until_scroll: Arc::new(Mutex::new(ticks_until_scroll)),
            remaining_ticks_at_begin: Arc::new(Mutex::new(ticks_at_begin)),
        }
    }
}

fn string_from_cached(cmds: &Vec<Arc<Cmd>>) -> String {
    let mut output = String::new();
    for cmd in cmds {
        // let cmd = cmd.clone();
        let cached = cmd.cached_output.clone().lock().unwrap().clone();
        if let Some(cached_str) = cached {
            let mut cur_output = String::new();

            let box_len = cmd.param.max_box_width as usize;
            if box_len != 0 && cached_str.len() > box_len {
                if cmd.param.is_scrolling {
                    let cached_str_pad = cached_str.clone() + "   ";
                    let mut skip_chars = cmd.cur_start_index.lock().unwrap();
                    if *skip_chars >= cached_str_pad.len() - box_len {
                        let nb_chars_before = cached_str_pad.len() - *skip_chars;
                        cur_output += &cached_str_pad
                            .chars()
                            .skip(*skip_chars)
                            .take(nb_chars_before)
                            .collect::<String>();
                        let nb_chars_after = box_len - nb_chars_before;
                        cur_output += &cached_str_pad
                            .chars()
                            .take(nb_chars_after)
                            .collect::<String>();
                    } else {
                        cur_output += &cached_str_pad
                            .chars()
                            .skip(*skip_chars)
                            .take(box_len)
                            .collect::<String>();
                    }

                    let mut ticks_at_begin = cmd.remaining_ticks_at_begin.lock().unwrap();
                    if *ticks_at_begin == 0 {
                        let mut ticks_till_scroll = cmd.remaining_ticks_until_scroll.lock().unwrap();
                        if *ticks_till_scroll == 0 {
                            if *skip_chars >= cached_str_pad.len() - 1 {
                                *skip_chars = 0;
                                *ticks_at_begin = cmd.param.ticks_at_begin;
                            } else {
                                *skip_chars += 1;
                            }
                            *ticks_till_scroll = cmd.param.ticks_until_scroll;
                        } else {
                            *ticks_till_scroll -= 1;
                        }
                    } else {
                        *ticks_at_begin -= 1;
                    }
                } else { //not scrolling
                    cur_output += &cached_str
                        .chars()
                        .take(box_len - 3)
                        .collect::<String>();
                    cur_output += "...";
                }
            } else { //box large enough for output
                cur_output += &cached_str;
            }


            if cached_str.len() > 0 {
                output += if output.len() > 0 { " | " } else { " " }
            }
            output += &cur_output;
        }
    }
    output += " ";

    output
}

fn update_bar(out: &str) {
    Command::new("xsetroot")
        .args(["-name", out])
        .status()
        .expect("Failed to update status bar");
}

fn run_loop_cmd(cmd: Arc<Cmd>) -> ! {
    loop {
        if let Ok(out) = Command::new(&cmd.param.cmd).args(cmd.param.args.as_slice()).output() {
            let out = out.stdout;
            let out_str = match std::str::from_utf8(out.as_slice()) {
                Ok(s) => s.trim().to_string(),
                Err(_) => out.escape_ascii().to_string()
            };

            {
                let mut cur_output = cmd.cached_output.lock().unwrap();
                if cmd.param.is_scrolling {
                    if let Some(ref str) = *cur_output {
                        if *str != out_str {
                            *cmd.cur_start_index.lock().unwrap() = 0;
                            *cmd.remaining_ticks_at_begin.lock().unwrap() = cmd.param.ticks_at_begin;
                        }
                    }
                }
                *cur_output = Some(out_str);
            }

        } else { //error running cmd
            //TODO: Add optional error message
        }

        if cmd.param.ms_between_refresh > 0 {
            thread::park_timeout(Duration::from_millis(cmd.param.ms_between_refresh));
        }
    }
}


static mut MQ_FD: libc::mqd_t = -1;

struct Cleanup { }

fn close_msg_queue() {
    //Safe because it's the only function that closes the MD_FD
    unsafe {
        if MQ_FD != -1 {
            if libc::mq_close(MQ_FD).is_negative() || libc::mq_unlink(MSG_QUEUE_NAME.as_ptr()).is_negative() {
                eprintln!("rsbar: Failed to close message queue.\nRun 'rm /dev/mqueue/<queuename>' manually");
            }
        }
    }
}

impl Drop for Cleanup {
    fn drop(&mut self) {
        close_msg_queue();
    }
}


//Necessary to use MaybeUninit since mq_attr has private members
fn init_mq_attr() -> libc::mq_attr {
    //Safe because a libc::mq_attr is valid when zeroed
    unsafe { MaybeUninit::<libc::mq_attr>::zeroed().assume_init() }
}

fn init_message_queue() -> Cleanup {
    //Safe, because all opening/closing of the message queue (MD_FD) are in here for the case
    //where the program refreshes the arguments
    unsafe {
        MQ_FD = libc::mq_open(MSG_QUEUE_NAME.as_ptr(), libc::O_RDONLY | libc::O_CREAT | libc::O_EXCL, 0o777, std::ptr::null() as *const c_void);
        if MQ_FD.is_negative() {
            eprintln!("Failed to start message queue. Make sure it doesn't already exist and is owned by another rsbar `daemon`. errno: '{}'", errno().0);
            process::exit(1);
        }
    }

    //TODO: see if I can remove this dependency
    if let Err(e) = ctrlc::set_handler(|| {
        close_msg_queue();
        process::exit(1);
    }) {
        close_msg_queue();
        panic!("Failed to setup termination handler with err: '{}'", e);
    }

    Cleanup { }
}

//Opens and sends
fn read_mq() -> String {
    unsafe {
        let mut attr = init_mq_attr();
        if libc::mq_getattr(MQ_FD, &mut attr).is_negative() {
            panic!("Failed to get attr: errno: '{}'", errno().0);
        }
        let string = libc::calloc(1, attr.mq_msgsize as usize) as *mut c_char;
        if string.is_null() { panic!("Failed libc::calloc: errno: '{}'", errno().0); }

        let mut msg_prio = 0;
        let _bytes_read: libc::ssize_t = libc::mq_receive(MQ_FD, string, attr.mq_msgsize as usize, &mut msg_prio);
        let cstr = core::ffi::CStr::from_ptr(string as *const i8);

        //sent string needs to always be a valid null-terminated UTF-8 C string
        let ret = String::from(cstr.to_str().unwrap());

        libc::free(string as *mut c_void);

        ret
    }
}

//Opens and sends
fn send_mq(args: Vec<String>) {
    unsafe {
        MQ_FD = libc::mq_open(MSG_QUEUE_NAME.as_ptr(), libc::O_WRONLY);
        if MQ_FD.is_negative() {
            panic!("Failed to open message queue. Is rsbar running? errno: '{}'", errno().0);
        }

        for arg in args {
            let cstr = std::ffi::CString::new(arg).unwrap();
            let ret = libc::mq_send(MQ_FD, cstr.as_ptr(), cstr.as_bytes().len(), 0);
            if ret.is_negative() {
                panic!("Failed to send message: ret: '{ret}', errno: '{}'", errno().0);
            }
        }
    }
}


fn main() {

    //TODO: add parsing commands from file paths
    let matches = clap::Command::new("rsbar")

        .arg(
            Arg::new("kill")
                .help("Terminates gracefully the current background instance of 'rsbar'")
                .short('k')
                .long("kill")
                .action(clap::ArgAction::SetTrue)
                .exclusive(true)
        )

        .arg(
            Arg::new("quick_update")
                .help("Makes the target commands rerun immediately")
                .short('u')
                .long("quick_update")
                .num_args(1..)
                .exclusive(true)
        )

        .arg(
            Arg::new("refresh_time")
                .help("Sets the refresh for the display in milliseconds")
                .short('r')
                .long("refresh")
                .default_value("0")
                .value_parser(clap::value_parser!(u64))
        )
        .arg(
            Arg::new("command")
                .long("command")
                .num_args(1..)
                .allow_hyphen_values(true)
                .value_terminator("--end_command")
                .action(clap::ArgAction::Append)
                .required(true)
        )
        .get_matches();

    let cmd_parser = clap::Command::new("--command")
        .arg(
            Arg::new("name")
                .short('n')
                .long("name")
                .help("Sets the name of the command (can be shared between commands)")
                .required(true)
        )
        .arg(
            Arg::new("cmd")
                .short('c')
                .long("cmd")
                .help("Tells rsbar which binary to run (looks at PATH)")
                .required(true)
        )
        .arg(
            Arg::new("args")
                .short('a')
                .long("args")
                .help("Arguments to add to the command (in execve)")
                .num_args(0..)
                .allow_hyphen_values(true)
                .value_terminator("--end_args")
        )
        //TODO
        .arg(
            Arg::new("timeout_ms")
                .short('t')
                .long("timeout_ms")
                .help("The maximum amount of time that the command should run for (todo)")
                .default_value("0")
                .value_parser(clap::value_parser!(u64))
        )
        //TODO
        .arg(
            Arg::new("is_scrolling")
                .short('s')
                .long("scroll")
                .action(clap::ArgAction::SetTrue)
                .help("Flag to enable scrolling for the box")
                .required(false)
        )
        //TODO: set this as dependant of is_scrolling
        .arg(
            Arg::new("ticks_until_scroll")
                .short('u')
                .long("ticks")
                .help("If scrolling, waits xxx ticks before scrolling one character")
                .default_value("10")
                .required(false)
                .value_parser(clap::value_parser!(u64))
        )
        .arg(
            Arg::new("ticks_at_begin")
                .short('b')
                .long("ticks_begin")
                .help("If scrolling, waits xxx ticks before starting to scroll at the beginning")
                .default_value("50")
                .required(false)
                .value_parser(clap::value_parser!(u64))
        )
        //TODO
        .arg(
            Arg::new("ms_between_refresh")
                .short('r')
                .long("ms_between_refresh")
                .help("How long rsbar waits to rerun the command")
                .default_value("0")
                .value_parser(clap::value_parser!(u64))
        )
        .arg(
            Arg::new("max_box_width")
                .short('w')
                .long("max_box_width")
                .help("Maximum width of the command box")
                .default_value("0")
                .value_parser(clap::value_parser!(u64))
        );

    //Quicky kills the current rsbar instance and exits
    if matches.get_flag("kill") {
        send_mq(vec!("RSBAR_STOP".to_string()));
        process::exit(0);
    }

    //Updates mq and exits
    if let Some(quick_update_args) = matches.get_many::<String>("quick_update") {
        let args: Vec<String> = quick_update_args.map(|e| e.to_string()).collect::<Vec<_>>();
        send_mq(args);
        process::exit(0);
    }

    let args_it = matches.get_occurrences("command").expect("'--command' is required");

    let args_vec: Vec<Vec<String>> = args_it.map(|it| {
        let mut vec: Vec<String> = vec!("--command".to_owned());
        it.for_each(|e: &String| vec.push(e.clone()));
        vec
    }).collect();

    let mut name_to_index: HashMap<String, IntOrVecInt> = HashMap::new();
    let cmds = args_vec.iter().enumerate()
        .map(|(i, args)| {
            let matches_cmd = cmd_parser.clone().get_matches_from(args);
            let cmd = Cmd::new(matches_cmd);

            //If None, append value to the vector present
            let value_to_insert: Option<IntOrVecInt> = if let Some(e) = name_to_index.get_mut(&cmd.param.name.clone()) {
                match e {
                    IntOrVecInt::Int(int) => { Some(IntOrVecInt::Vec(vec!(*int, i))) },
                    IntOrVecInt::Vec(_) => { /*vector exists, append*/ None }
                }
            } else {
                Some(IntOrVecInt::Int(i))
            };

            if let Some(val) = value_to_insert {
                name_to_index.insert(cmd.param.name.clone(), val);
            } else {
                if let IntOrVecInt::Vec(vec) = name_to_index.get_mut(&cmd.param.name.clone()).unwrap() {
                    vec.push(i);
                } else { unreachable!() }
            }

            Arc::new(cmd)
    }).collect();


    //TODO: incorrect behaviour when several commands have the same name
    let state = State {
        overall_refresh_ms: *matches.get_one::<u64>("refresh_time").unwrap(),
        cmds,
        name_to_index
    };

    //closes the mq_when it goes out of scope
    //also closes it manually when getting a SIGTERM/SIGINT and drop isn't run
    let _cleanup = init_message_queue();

    let _cmd_handles: Vec<_> = state.cmds.iter()
        .map(|cmd| {
            let cmd = (*cmd).clone();
            thread::spawn(move || { run_loop_cmd(cmd); })
        }).collect();

    let update_queue = Arc::new(Mutex::new(VecDeque::<String>::new()));
    let og_thread = thread::current();
    let update_queue_writer = update_queue.clone();
    let _msq_q_checker = thread::spawn(move || {
        loop {
            //Blocks until gets message (or stays blocked forever)
            let msg = read_mq();
            if msg == "RSBAR_STOP" {
                close_msg_queue();
                process::exit(0);
            }
            {
                let mut queue = update_queue_writer.lock().unwrap();
                (*queue).push_back(msg);
            }
            og_thread.unpark();
        }
    });

    //TODO: I think there's a slight bug, where the command thread is unparked right before the
    //update thread, and doesn't have the time to update its output before the next refresh, which
    //will threfore be shown on the next available refresh
    let update_queue_reader = update_queue.clone();
    loop {
        //Check if there's stuff on the update_queue
        { //Scoped mutex
            let mut queue = update_queue_reader.lock().unwrap();
            for elem in (*queue).drain(..) {
                if let Some(e) = state.name_to_index.get(&elem) {
                    match e {
                        IntOrVecInt::Int(pos) => {
                            _cmd_handles[*pos].thread().unpark();
                        },
                        IntOrVecInt::Vec(vec) => {
                            for pos in vec.iter() {
                                _cmd_handles[*pos].thread().unpark();
                            }
                        }
                    }
                } else {
                    eprintln!("Failed to update '{}'!", &elem);
                }
            }
        } //release Mutex
        let out = string_from_cached(&state.cmds);
        update_bar(&out);
        if state.overall_refresh_ms > 0 {
            thread::park_timeout(Duration::from_millis(state.overall_refresh_ms));
        }
    }

    // for handle in _cmd_handles {
    //     handle.join().unwrap();
    // }
}
